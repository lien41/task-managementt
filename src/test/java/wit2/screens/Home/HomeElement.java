package wit2.screens.Home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomeElement {

    @FindBy(id = "g2599-name")
    protected WebElement name;

    @FindBy(id = "g2599-email")
    protected WebElement email;

    @FindBy(id = "g2599-website")
    protected WebElement website;

   @FindBy(id = "g2599-experienceinyears")
    protected WebElement experience;

    public List<WebElement> getExperiences(){
        List<WebElement> listExperience = experience.findElements(By.tagName("option"));
        return listExperience;
    }

    @FindBy(name = "g2599-expertise[]")
    protected WebElement expertise;
    public List<WebElement> getExpertise(){
        List<WebElement> listExpertise = expertise.findElements(By.tagName("label"));
        return listExpertise;
    }

    @FindBy(name = "g2599-education")
    protected WebElement education;

    /*public List<WebElement> getEducation(){
        List<WebElement> listEducation = education.findElements(By.tagName("label"));
        return listEducation;*/
}
